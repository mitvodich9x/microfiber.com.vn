﻿//custom slick slider
$(".slider").slick({
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>'
});

$('.slide').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    dots:false,
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false,
        }
    },
    {
        breakpoint: 600,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
});

//Scroll to Top
$(window).scroll(function () {
    if ($(this).scrollTop() > 2000) {
        $('.click-to-top').fadeIn(200);
    } else {
        $('.click-to-top').fadeOut(200);
    }
});
$('.click-to-top').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 500);
});

//Menu mobile
console.clear()

const navExpand = [].slice.call(document.querySelectorAll('.nav-expand'))
const backLink = `<li class="nav-item">
	<a class="nav-link nav-back-link" href="javascript:;">
		Back
	</a>
</li>`

navExpand.forEach(item => {
    item.querySelector('.nav-expand-content').insertAdjacentHTML('afterbegin', backLink)
    item.querySelector('.nav-link').addEventListener('click', () => item.classList.add('active'))
    item.querySelector('.nav-back-link').addEventListener('click', () => item.classList.remove('active'))
})

const ham = document.getElementById('ham')
ham.addEventListener('click', function () {
    document.body.classList.toggle('nav-is-toggled')
})
